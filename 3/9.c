#include <stdio.h>
int flag = 0;

void clear (int n, int visited[n]){
    for (int i = 0; i < n; i++) visited[i] = 0;
}


void printNode(int n, int a[n][n], int visited[n], int row){
    if (visited[row] == 1) return;
    visited[row] = 1;

    for (int j = 0; j < n; j++){
        if (a[row][j] == 1) printNode(n,a,visited,j);
    }
}

int main(){
    int n,e;
    scanf("%d %d", &n, &e);

    int a[n][n];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }

    int u,v;
    for (int i = 0; i <e; i++){
        scanf("%d %d", &u, &v);
        a[u-1][v-1] = 1;
    }

    int visited[n];
    int f;

    for (int i = 0; i < n; i++){
        clear(n,visited);
        f = 0;
        printNode(n,a,visited,i);
        for (int j = 0; j< n; j++){
            if (visited[j] == 0){
                f = 1;
                break;
            }
        }
        if (f == 0) printf("%d ", i+1);
    }
}
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main(){
    char s[11];
    scanf("%s", s);

    int l = strlen(s);

    int a[26];
    for (int i = 0; i < 26; i++) a[i] = 0;
    for (int i = 0; i < l; i++) a[s[i] - 97] += 1;

    int count_odd = 0;

    if (l%2 == 0){
        for (int i = 0; i < 26; i++){
            if (a[i]%2 == 1) {
                printf("No\n");
                exit(0);
            }
        }
    }else {
        for (int i = 0; i < 26; i++){
            if (a[i]%2 == 1){
                count_odd += 1;
                if (count_odd >= 2) {
                    printf("No\n");
                    exit(0);
                }
            }
        }
    }
    printf("Yes\n");
}
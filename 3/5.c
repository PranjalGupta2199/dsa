#include <stdio.h>
int max = 0;

int count (int n, int a[n][n], int b[n][n], int row, int col, int c){
    for (int i = -1; i <= 1; i++){
        if (row + i >= 0 && row + i < n){
            for (int j = -1; j <= 1; j++){
                if ( col + j >= 0 && col + j < n ){
                    if (a[row+i][col+j] == 1 && b[row+i][col+j] == 0){
                        b[row+i][col+j] = 1;
                        count(n,a,b,row+i,col+j,c + 1);
                    }                 
                }
            }
        }        
  
    }
    if (c > max) max = c;
}

void clear(int n, int b[n][n]){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) b[i][j] = 0;
    }
}


int main(){
    int n;
    scanf("%d", &n);

    int a[n][n];
    for (int i = 0; i< n; i++){
        for (int j = 0; j < n; j++) scanf("%d", &a[i][j]);
    }
    
    int b[n][n];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            clear(n,b);
            count(n,a,b,i,j,0);
        }
    }
    printf("%d\n", max);

}
#include <stdio.h>

int main(){
    char s[1001];
    scanf("%s", s);

    int m;
    scanf("%d", &m);

    int f,r,k;
    int count;
    for (int i = 0; i < m; i++){
        scanf("%d %d %d", &f, &r, &k);
        f --;
        r --;
        count = k%(r-f+1);

        for (int o = 0; o < count; o++){
            char c = s[r];
            for (int j = r; j > f; j--){
                s[j] = s[j-1];
            }
            s[f] = c;
        }
    }
    printf("%s\n",s);
}
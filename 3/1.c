#include <stdio.h>
#include <string.h>

int check(char s[1001], char p[1001]){
    int l = strlen(s);
    int count = 0;
    for (int i = 0; i < l; i++){
        if (s[i] == p[count]){
            count ++;
        }
    }
    if (count == strlen(p)) return 1;
    else return 0;
}

int remove_char(int n, int a[n], char s[1001], char p[1001]){
    for (int i = 0; i < n; i++){
        s[a[i]-1] = '0';
        if (check(s,p) == 0) return i;
    }
}

int main(){
    char s[1001], p[1001];
    scanf("%s", s);
    scanf("%s", p);

    int n;
    scanf("%d", &n);
    int a[n];
    for (int i = 0; i < n; i++) scanf("%d", &a[i]);

    int max = remove_char(n,a,s,p);
    printf("%d\n",max);

}
#include <stdio.h>
#include <stdlib.h>

int n = 0;

typedef struct nodeType{
    int n;
    struct nodeType *next;
}node;

void create (node **head){
    node *head_ref = *head;
    int x;
    char c;
    do{
        scanf("%d%c", &x, &c);
        node *tmp = (node *)malloc(sizeof(node));
        tmp->n = x;
        if (n == 0){
            *head = tmp;
            head_ref = tmp;
        }
        else head_ref->next = tmp;
        head_ref = tmp;
        n ++;
        if (c == '\n') return;
    }
    while (1);
}

node * reverseList(node *head){
    node *curr = head;
    node *succ = head->next;

    int count = 0;
    while (succ != NULL){
        node *tmp = succ;
        succ = succ->next;
        
        tmp ->next = curr;
        if (count == 0) curr->next = NULL;
        curr = tmp;
        count ++;
    }
    return curr;
}

void printList(node *head, int k){
    int count = n/k;
    node *head_ref = head;
    for (int i = 1; i <= n; i++){
        if (i%k == 0) printf("%d ", head_ref->n);
        head_ref = head_ref->next;
    }

    head_ref = reverseList(head);
    while (head_ref != NULL && n != 0){
        if (n%k != 0) printf("%d ", head_ref->n);
        n --;
        head_ref = head_ref->next;
    }
    
}

int main(){
    int k;
    scanf("%d", &k);

    node * head = NULL;
    create(&head);

    printList(head,k);
}
#include <stdio.h>
int reached = 0;
int x;
int y;
int flag_x = 0;
int flag_y = 0;

void clear (int n, int stack[n], int visited[n]){
    flag_y = -1;
    reached = 0;
    flag_x = -1;
    for (int i = 0; i < n; i++){
        visited[i] = 0;
        stack[i] = -1;
    }
}

void connect(int n, int a[n][n], int visited[n], int stack[n], int row, int col, int counter){
    if (visited[row] == 1) return;
    visited[row] = 1;

    stack[counter] = row;
    

    if (a[row][col] == 1) {
        stack[counter + 1] = col;
        reached = 1;

        return;
    }
    for (int i = 0; i < n; i++){
        if (a[row][i] == 1){
            connect(n,a,visited,stack, i,col, counter +1);
            if (reached == 1) return;  
        }
    }
}


int main(){
    int n;
    scanf("%d %d %d", &n, &x, &y);
    x--;
    y--;

    int a[n][n];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }

    for (int i = 0; i < n-1;i++){
        int u,v;
        scanf("%d %d", &u, &v);
        a[u-1][v-1] = 1;
        a[v-1][u-1] = 1;
    }

    int visited[n], stack[n];
    int ans = 0;

    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            clear(n,stack, visited);
            if (i != j) {
                connect(n,a,visited,stack, i,j,0);
                for (int k = 0; k < n; k++){
                    if (stack[k] == x) flag_x = k;
                    else if (stack[k] == y) flag_y = k;
                }
                if (flag_x == -1 || flag_y == -1 || flag_y < flag_x) {
                    ans++;
                    printf("%d %d\n",i+1,j+1 );
                }
            }
        }
    }
    printf("%d\n", ans);
}
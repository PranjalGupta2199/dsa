#include <stdio.h>
#include <stdlib.h>
int n = 0;

typedef struct nodeType{
    int n;
    struct nodeType * next;
}node;

void printList(node *head){
    while (head != NULL){
        printf("%d ",head->n );
        head = head->next;
    }
}

void create(node **head1, node ** head2, int k){
    node *head_ref1 = *head1;
    node *head_ref2 = *head2;

    char c;
    int in;
    do{
        scanf("%d%c", &in, &c);
        node *tmp = (node *)malloc(sizeof(node));
        tmp->n = in;
        n++;
        if (n%k == 0){
            if (n == k){
                *head2 = tmp;
                head_ref2 = tmp;
            }
            else head_ref2->next = tmp;
            head_ref2 = tmp;
        }
        else {
            if (n == 1){
                *head1 = tmp;
                head_ref1 = tmp;
            }
            else head_ref1->next = tmp;
            head_ref1 = tmp;
        }
        if (c == '\n') return;
    }
    while (1);

}

node * reverse (node *head1){
    int counter = 0;

    node *curr = head1;
    node *succ = head1->next;

    while (succ != NULL){
        node *tmp = succ;
        succ = succ->next;

        tmp->next = curr;
        if (counter == 0) curr->next = NULL;
        curr = tmp;
        counter ++;
    }

    return curr;


}

void join (node *head1, node * head2){
    node * head_ref = head2;

    if (head_ref != NULL) {
        while (head_ref->next != NULL) head_ref = head_ref->next;
        head_ref->next = head1;
    }

    if (head2 != NULL) printList(head2);
    else printList(head1);
}



int main(){
    node *head1 = NULL;
    node *head2 = NULL;

    int k;
    scanf("%d", &k);

    create(&head1, &head2, k);

    head1 = reverse(head1);

    join(head1, head2);
}
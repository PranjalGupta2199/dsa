#include <stdio.h>

void clear(int n, int visited[n]){
    for (int i = 0; i < n; i++) visited[i] = 0;
}


void checkPath(int n, int a[n][n], int visited[n], int u, int v){
    if (visited[u] == 1) return;
    if (a[u][v] == 1){
        visited[u] = 1;
        visited[v] = 1;
        return;
    }

    visited[u] = 1;

    for (int i = 0; i < n; i++){
        if (a[u][i] == 1) checkPath(n,a,visited,i,v);
    }

}

int main(){
    int n,e;
    scanf("%d %d", &n, &e);

    int a[n][n];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }

    for (int i = 0; i < e; i++){
        int u,v;
        scanf("%d %d", &u, &v);
        a[u-1][v-1] = 1;
    }

    int t;
    scanf("%d", &t);
    int visited[n];

    for (int i = 0; i < t; i++){
        int x,y;
        scanf("%d %d", &x, &y);

        clear(n,visited);

        checkPath(n,a,visited,x-1,y-1);
        if (visited[x-1] == 1 && visited[y-1] == 1)printf("Yes\n");
        else printf("No\n");

    }
}


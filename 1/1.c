#include <stdio.h>
int min(int n, int a[n][n], int r){
    int e = a[r][0];
    
    for (int i = 1; i < n; i++){
        if (a[r][i] < e) e = a[r][i];
    }
    return e;
}

int max(int n, int a[n][n], int c){
    int e = a[0][c];
    
    for (int i = 1; i < n; i++){
        if (a[i][c] > e) e = a[i][c];
    }
    return e;
}


int main(){
    int n;
    scanf("%d", &n);
    
    int a[n][n];
    
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            scanf("%d", &a[i][j]);
        }
    }
    int flag = 0;
    for (int i = 0; i < n; i++){
        int e = min(n,a,i);
        for (int j = 0; j < n; j++){
            if (e == max(n,a,j)) {
                printf ("%d\n", a[i][j]);
                flag = 1;
            }
        }
    }
    if (flag == 0) printf("No saddle points\n");
    
}

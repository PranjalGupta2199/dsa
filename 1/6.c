#include <stdio.h>
int main(){
    int a,b;
    scanf("%d%d", &a, &b);
    
    int p[b+1];
    for (int i = 0; i< b; i++) p[i] = 1;
    
    for (int i = 2; i <= b; i++){
        int j = 2*i;
        while (j < b){
            if (p[j] == 1) p[j] = 0;
            j += i; 
        }
    }
    for (int i = a; i <= b; i++){
        if (p[i] == 1) printf("%d ", i);
    } 
}

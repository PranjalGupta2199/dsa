#include <stdio.h>
#include <stdlib.h>
typedef struct nodeType{
    int v;
    struct nodeType *next;
}node;
int n = 0;

void create(node **head){
    node *head_ref;
    head_ref = NULL;
    
    int in;
    char c;
    do{
        scanf("%d%c", &in, &c);
        node *tmp = (node *)malloc(sizeof(node));
        tmp->v = in;
        if (n == 0 && c == '\n'){
            *head = tmp;
            head_ref = tmp;
            n++;
            break;
        }
        else if (n == 0){
            *head = tmp;
            head_ref = tmp;
        }
        else{
            head_ref->next = tmp;
            head_ref = tmp;
        }
        n++;
        if (c == '\n')  {
            head_ref->next = *head;
            return;
        }
    }
    while (1);
} 

void print (node * head, int k){
    int c = n - k + 1;
    printf("\n%d\n",c );
    if (c <= 0) {
        printf("No elements available\n");
    }
    else{
        while (head != NULL && c != 1){
            head = head->next;
            c -= 1;
        }
        if (head == NULL){
            printf ("Error ! Head is null\n");
        }
        else{
            printf ("%d\n", head->v);
        }
    }
}


int main(){
    node * head = NULL;
    create(&head);
    
    int k;
    scanf("%d", &k);
    print (head, k);
        


}

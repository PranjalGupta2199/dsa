#include <stdio.h>

void spiral (int r, int c, int n, int a[n][n], int count, int index, int *prime){
	if (n%2 == 0){
		if (count == n/2 ) return;
		a[r][c] = prime[index];
		while (c != n - 1 - count){
			c += 1;
			index -= 1;
			a[r][c] = prime[index];
		}
		while (r != n -1 -count){
			r += 1;
			index -= 1;
			a[r][c] = prime[index];
		}
		while (c != count){
			c -= 1;
			index -= 1;
			a[r][c] = prime[index];
		}
		while (r != count + 1){
			r -= 1;
			index -= 1;
			a[r][c] = prime[index];
		}
		spiral(r, c + 1, n, a, count + 1, index - 1, prime);
	}
	else{
		if (count == n/2) {
			a[count][count] = 1;
			return;
		}
		int R = r;
		int C = c;
		a[r][c] = prime[index];

		while (c != count){
			c -= 1;
			index -= 1;
			a[r][c] = prime[index];
		}
		while (r != count ){
			r -= 1;
			index -= 1;
			a[r][c] = prime[index];
		}
		while (c != C ){
			c += 1;
			index -= 1;
			a[r][c] = prime[index];	
		}
		while (r != R - 1){
			r += 1;
			index -= 1;
			a[r][c] = prime[index];
		}
		spiral(r, c - 1, n, a, count + 1, index - 1, prime);
	}
}

int main(){
	int p[1000000];
	for (int i = 0; i < 1000000; i++) p[i] = 1;
	for (int i = 2; i < 1000000; i++){
		int j = 2*i;
		while (j <= 1000000){
			if (p[j] == 1) p[j] = 0; 
			j+= i;
		}
	}
	int c[100000];
	for (int i = 0; i < 100000; i++) c[i] = 0;
	int counter = 0;
	for (int i = 2; i < 1000000; i++){
		if (p[i] == 1) {
			c[counter] = i;
			counter ++;
		}
	}

	int n;
	scanf("%d", &n);
	int a[n][n];
	for (int i = 0; i < n; i++) {
		for (int j = 0; j < n; j++){
			a[i][j] = 0;
		}
	}
	if (n%2 == 0) spiral (0,0, n, a,0, n*n - 1, c);
	else spiral (n-1, n-1, n, a,0, n*n - 1, c);
	
	for (int i = 0; i< n; i++){
		for (int j = 0; j < n; j++){
			printf("%d\t", a[i][j]);
		}
		printf("\n");
	}
}
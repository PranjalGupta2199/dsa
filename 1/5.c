#include <stdio.h>
#include <stdlib.h>
int n = 0;
typedef struct node
{
	int n;
	struct node * next;
}node;


void create(node **head){
	node *head_ref = *head;
	int input;
	char c;
	do{
		scanf("%d%c", &input, &c);
		node * tmp = (node *)malloc(sizeof(node));
		tmp->n = input;
		if (n == 0){
			*head = tmp;
			head_ref = tmp;
		}
		else{
			head_ref->next = tmp;
			head_ref = tmp;
		}
		n++;
		if (c == '\n') return;
	}
	while (1);
}

void search (node *head, int x){
	int counter = 0;
	while (head != NULL){
		if (head->n == x){
			printf("%d\n", counter);
		}
		counter ++;
		head = head->next;
	}
	return;
}

void print (node *head){
	while (head->next != NULL){
		printf("%d->", head->n);
		head = head->next;		
	}
	printf("%d\n",head->n);
}

void delete(node *head, int x){
	int counter = 0;
	node *head_ref = head;
	node *prev = head;

	if (head->n == x){
		counter ++;
		head = head->next;
	}
	while (head != NULL){
		if (head->n == x){
			if (counter == 0) {
				counter ++;
				head = head->next;
			}
			else {
				prev->next = head->next;
				node *d = head;
				head = head->next;
				free(d);
				counter ++;
			}
		}
		else{
			prev = head;
			head = head->next;
		}
	}
	if (counter == 0) printf("No elements found\n");
	print (head_ref);
}

void swap(node *head, int x){
	node *head_ref = head;
	int counter = 0;
	if (head->n == x) {
		int v = head->n;
		head->n = head->next->n;
		head->next->n = v;
	}
	else{
		while (head != NULL){
			if (counter == 0 && head->n == x){
				int v = head->n;
				head->n = head->next->n;
				head->next->n = v;
				break;
			}
			head = head->next;
		}
	}
	print (head_ref);
}

int main(){
	node * head = NULL;
	create(&head);
	search(head, 1);
	swap(head,2);
	delete(head, 10);

}
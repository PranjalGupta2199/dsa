#include <stdio.h>
#include <stdlib.h>
typedef struct nodeType{
    int v;
    struct nodeType *next;
    struct nodeType *prev;
}node;

void create(node **head, int n){
    node *head_ref;
    head_ref = NULL;
    
    for (int i = 0; i < n; i++){
        node *a = (node *) malloc(sizeof(node));
        scanf("%d", &a->v);
        if (i == 0) {
            *head = a;
            head_ref = a;
            
        }else{
            head_ref->next = a;
            a->prev = head_ref;
            head_ref = a;
        }            
    }
} 

void print (node * head, int n, int k){
    int c = n - k + 1;
    if (c <= 0) {
        printf("No elements available\n");
    }
    else{
        while (head != NULL && c != 1){
            head = head->next;
            c -= 1;
        }
        if (head == NULL){
            printf ("Error ! Head is null\n");
        }
        else{
            printf ("%d\n", head->v);
        }
    }
}


int main(){
    int n;
    scanf("%d", &n);
   
    node * head = NULL;
    create(&head, n);
    
    int k;
    scanf("%d", &k);
    print (head, n,k);
        


}

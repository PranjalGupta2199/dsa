#include <stdio.h>

void spiral(int r, int c, int v, int n, int a[n][n], int count){
    int C = c - 1;
    int R = r - 1;

    if (n %2 == 0){
    	if (count == n/2){
    		return;
    	}
        a[r][c] = v;
        while (c != n - 1 - count){
            c += 1;
            v -= 1;
            a[r][c] = v;
        }
        while (r != n - 1 - count){
            r += 1;
            v -= 1;
            a[r][c] = v;
        }
        while (c != count + 1){
            c -= 1;
            v -= 1;
            a[r][c] = v;   
        }
        while (r != count + 1){
            r -= 1;
            v -= 1;
            a[r][c] = v;
        }
        spiral(r, c+1, v - 1, n,a,count + 1);
    }
    else {
    	if (count == n/2) {
        	a[count][count] = 1;
        	return;
    	} 
	    a[r][c] = v;
	    while (c != count){
	        c -= 1;
	        v -= 1;
	        a[r][c] = v;
	    }
	    while (r != count){
	        r -= 1;
	        v -= 1;
	        a[r][c] = v;
	    }
	    while (c != C + 1){
	        c += 1;
	        v -= 1;
	        a[r][c] = v;
	    }
	    while (r != R ){
	        r += 1;
	        v -= 1;
	        a[r][c] = v;
	    }
    	spiral(R, C, v - 1, n, a, count+1);
    }
    
}

int main(){
    int n;
    scanf("%d", &n);
    
    int a[n][n];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }
    
    if (n%2 == 0) spiral(0, 0,n*n, n,a, 0);
    else spiral(n -1, n -1, n*n, n, a, 0);
    
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            printf("%d\t", a[i][j]);
        }
        printf("\n");
    }
}


#include <stdio.h>
#include <stdlib.h>
int n = 0;

typedef struct nodeType{
    int v;
    struct nodeType *next;
}node;

void create(node **head){
    node *head_ref;
    head_ref = NULL;
     
    int v;
    char ch; 
    do{ 
        scanf("%d%c", &v, &ch);
        node *a = (node *) malloc(sizeof(node));
        a->v = v;
        if (n == 0) {
            *head = a;
            head_ref = a;
        }else{
            head_ref->next = a;
            head_ref = a;
        }
        n++;   
        if (ch == '\n') return;
    }
    while (1);
} 

void print (node * head, int k){
    int c = n - k + 1;
    if (c <= 0) {
        printf("No elements available\n");
    }
    else{
        while (head != NULL && c != 1){
            head = head->next;
            c -= 1;
        }
        if (head == NULL){
            printf ("Error ! Head is null\n");
        }
        else{
            printf ("%d\n", head->v);
        }
    }
}

void traverse(node *head){
    while (head != NULL){
        printf("%d ", head->v);
        head = head->next; 
    }
    return;
}

int main(){
    printf ("Enter elements :\n");   
    node * head = NULL;
    create(&head);
    printf("Enter kth node :\n");
    int k;
    scanf("%d", &k);
    print (head, k);
    //traverse(head);     


}

#include <stdio.h>

int main(){
    int n;
    scanf("%d", &n);

    int a[n], count[n];
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
        count[i] = 0;
    }

    for (int i = 0; i < n; i++){
        int temp = 0;

        int min = a[i];
        for (int j = i+1; j < n; j++){
            if (a[j] >= min) temp += a[j] - min;
            else if (a[j] < min) min = a[j];
        }

        int max = a[i];
        for (int j = i-1; j >= 0; j--){
            if (a[j] < max) temp += max - a[j]; 
            else if (a[j] > max) max = a[j];
        }
        count[i] = temp;
    }

    int mini = 1000;
    for (int i = 0; i < n; i++){
        if (count[i] < mini) mini = count[i];
    }
    printf("%d\n", mini);
}
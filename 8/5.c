#include <stdio.h>
int n = 0;
#define MAX  1000

void add(int a[MAX]){
    char c;
    do{ 
        scanf("%d%c", &a[n], &c);
        n ++;
        if (c == '\n') return;
    }
    while (1);
}

void swap(int *a, int *b){
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

void sort(int n, int a[n]){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (a[i] > a[j]) swap(&a[i], &a[j]);
        }
    }
}

int main(){
    int a[MAX];
    add(a);
    sort(n,a);

    int carry = 0;
    int sum = 0;
    int power = 1;
    for (int i = 0; i < n - 1; i+= 2){
        int tmp = a[i] + a[i+1] + carry;
        carry = tmp/10;
        tmp %= 10;
        sum += power*tmp;
        power *= 10;
    }
    printf("%d\n", sum);
}
#include <stdio.h>

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void sort(int n, int a[n]){
    for (int i = 0; i  < n; i++){
        for (int j = 0; j < n; j++) {
            if (a[i] < a[j]) swap(&a[i], &a[j]);
        }
    }
}

int main(){
    int n,k;
    scanf("%d %d", &n, &k);

    int a[n];
    for (int i = 0; i < n; i++) scanf("%d", &a[i]);

    int flag = 0;
    int count = 0;
    int start = 0;
    do{
        sort(n,a);

        printf("Sort: ");
        for (int i = 0; i < n; i++) printf("%d ", a[i]);
        printf("\n");

        flag = 0;
        for (int i = start; i < n; i++){
            if (start == n-1 && a[start] < k) {
                printf("Not possible\n");
                break;           
            }
            if (a[i] < k){
                int temp = a[start] + 2*a[start+1];
                a[start+1] = temp;
                a[start] = -1;
                start ++;
                flag = 1;
                count ++;
                break;
            }
        }

    }
    while (flag != 0);

    printf("%d\n", count);
}
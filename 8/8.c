#include <stdio.h>

int main(){
    int n;
    scanf("%d", &n);

    int a[n][n];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) scanf("%d", &a[i][j]);
    } 


    int count = 0;
    int pt1,pt2,pt3,pt4;

    for (int pt1 = 0; pt1 < n; pt1++){
        for (int pt2 = pt1; pt2 < n; pt2++){
            for (int pt3 = 0; pt3 < n; pt3++){
                for (int pt4 = pt3; pt4 < n; pt4++){
                    int temp = 0;

                    for (int i = pt1; i <= pt2; i++){
                        for (int j = pt3; j <= pt4; j++) temp += a[i][j];
                    }
                    if (temp == 1) count  ++;
                    
                }
            }
        }
    }

    printf("%d\n", count);
    

}
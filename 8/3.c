#include <stdio.h>
#include <string.h>

int check(char *s, char *a, int top){
    int l = strlen(s);
    for (int i = 0; i < l; i++){
        if (top == -1){
            top ++;
            a[top] = s[i];
        }
        else {
            if (a[top] == '(' && s[i] == ')') a[top--] = '0';
            else if (a[top] == '{' && s[i] == '}') a[top--] = '0';
            else if (a[top] == '[' && s[i] == ']') a[top--] = '0';
            else if (a[top] == '<' && s[i] == '>') a[top--] = '0';
            else if (a[top] == '(' || a[top] == ')' || a[top] == '{' || a[top] == '}' || a[top] == '<' || a[top] == '>' || a[top] == '[' || a[top] == ']') {
                a[++top] = s[i];
            }
            else{}
        }
    }
    if (top == -1) return 1;
    else return 0;
}

int main(){
    char s[10000];
    scanf("%s", s);

    char a[10000];
    int top = -1;

    if (check(s,a,top) == 1) printf("True\n");
    else printf("False\n");
}
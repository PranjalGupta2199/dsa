#include <stdio.h>

void swap(int *a, int *b){
    int temp = *b;
    *b = *a;
    *a = temp;
}

void sort(int n, int a[n], int b[n]){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (a[i] < a[j]){
                swap(&a[i], &a[j]);
                swap(&b[i], &b[j]);
            }
            else if (a[i] == a[j]){
                if (b[i] < b[j]) swap(&b[i], &b[j]);
            }
        }
    }
}

int main(){
    int n;
    scanf("%d", &n);

    int at[n], pt[n];
    for (int i = 0; i < n; i++) scanf("%d %d", &at[i], &pt[i]);

    sort(n,at,pt);
    
    int wait_time[n];
    for (int i = 0; i < n; i++) wait_time[i] = 0;
    int curr_time = 0;

    for (int i = 0; i < n; i++){
        if (curr_time <= at[i]) curr_time = at[i];
        else {
            int min_index = i;
            int min = pt[i];
            for (int j = i; j < n; j++) {
                if (at[j] > curr_time) break;
                else {
                    if (pt[j] < min) {
                        min_index = j;
                        min = pt[min_index];
                    }
                }
            }
            swap(&at[min_index], &at[i]);
            swap(&pt[min_index], &pt[i]);
        }
        int proc_time = pt[i] + curr_time;
        wait_time[i] = proc_time - at[i];
        curr_time = proc_time;

    }   

    int sum = 0;
    for (int i = 0; i < n; i++) sum += wait_time[i];
    printf("%d\n", sum/n);
}
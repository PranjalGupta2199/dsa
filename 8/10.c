#include <stdio.h>

int min(int a,int b){
    if (a <= b) return a;
    else return b;
}

int main(){
    int n,k;
    scanf("%d %d\n", &n, &k);

    char a[n];
    for (int i = 0; i <n; i++) scanf("%c", &a[i]);

    int flag = 0;
    int count = 0;
    do{
        flag = 0;
        for (int i = 0; i < n; i++){
            if (a[i] != 'G'){
                for (int j = i; j < min(n,i+k); j++){
                    if (a[j] == 'R') a[j] = 'Y';
                    else if (a[j] == 'Y') a[j] = 'G';
                    else if (a[j] == 'G') a[j] = 'R';
                }
                count ++;
                flag = 1;
                break;
            }
        }
    }
    while (flag != 0);
    printf("%d\n", count);
}
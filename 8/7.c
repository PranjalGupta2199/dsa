#include <stdio.h>
int sum = 0;

int main(){
    int n;
    scanf("%d", &n);

    int a[n];
    for (int i = 0; i < n; i++) scanf("%d", &a[i]);
    int sum = 0;
    for (int i = 0; i < n; i++){
        for (int j = 0; j+i <n; j++){
            int m = 10000;
            for (int k = i; k <= j + i; k++) {
                if (a[k] < m) m = a[k];
            }
            sum += m;
        }
    }
    printf("%d\n", sum);
}
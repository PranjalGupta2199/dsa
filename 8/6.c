#include <stdio.h>
int a[10000];
int top = -1;
int min = 0;

void print(){
    for (int i = top; i >= 0; i--) printf("%d ", a[i]);
    printf("\n");
}

void push(){
    int e;
    scanf("%d", &e);

    if (top == 10000) printf("Overflow !\n");
    else {
        top ++;
        a[top] = e;
        if (a[min] > e) min = top;
    }
}

void pop(){
    if (top == -1) printf("Underflow !\n");
    else {
        printf ("Popped element is %d\n", a[top]);
        top --;
        int tmp = 0;
        for (int i = 0; i < top; i++) {
            if (a[tmp] > a[i]) tmp = i;
        }
        min = tmp;
    }
}

void mini(){
    if (top == -1) printf("No elements in the stack\n");
    else {
        printf("%d\n", a[min]);
    }
}


int main(){

    int ans = 0;
    while (ans != 4){
        print();
        printf("ENTER OPTION: \n");
        printf("1. PUSH\n");
        printf("2. POP\n");
        printf("3. MINIMUM\n");
        printf("4. EXIT\n");
        scanf("%d", &ans);
        if (ans == 1) push();
        else if (ans == 2) pop();
        else if (ans == 3) mini();
    }
}
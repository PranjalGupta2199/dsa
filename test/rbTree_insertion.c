#include <stdio.h>
#include <stdlib.h>
#define BLACK 1
#define RED 2
#define LEFT_CHILD 1
#define RIGHT_CHILD 2
int n = 0;

typedef struct node {
    struct node * left, *right, *parent;
    int data;
    int color;
}node;

void swap_color(node * a, node * b){
    int color = a->color;
    a->color = b->color;
    b->color = color;
}

node * insert_bstree(node *head, int value){
    node *head_ref = head;
    while (head_ref->left != NULL || head_ref->right != NULL){
        if (value <= head_ref->data){
            if (head_ref->left == NULL) return head_ref;
            else head_ref = head_ref->left;
        }
        else {
            if (head_ref->right == NULL) return head_ref;
            else head_ref = head_ref->right;
        }
    }
    return head_ref;
}

void left_rotate(node *x, node *y){
    if (y == NULL) return;
    x->right = y->left;
    y->left->parent = x;
    y->left = NULL;
    // add condition if x->parent == NULL, 
    // i.e add statement for root node
    if (x->parent->left == x){
        x->parent->left = y;
    }else {
        x->parent->right = y;
    }
    y->parent = x->parent;
    x->parent = y;
    y->left = x;
}

void right_rotate(node *x, node *y){
    if (y == NULL) return;
    x->left = y->right;
    y->left->parent = x;
    y->left = NULL;
    
    if (x->parent->left == x){
        x->parent->left = y;
    }else {
        x->parent->right = y;
    }
    y->parent = x->parent;
    x->parent = y;
    y->right = x;
}

void bstree_fixup(node *leaf){
    node * parent = leaf->parent;
    node * grandParent = NULL;
    node *uncle = NULL;
    int child_value = 0;
    int parent_value = 0;

    if (parent == NULL) {
        // root node is black
        leaf->color = BLACK;
        return;
    }
    else grandParent = parent->parent;

    if (grandParent == NULL) return;
    else {
        if (grandParent->right == parent){
            uncle = grandParent->left;
            parent_value = RIGHT_CHILD;
        }else {
            uncle = grandParent->right;
            parent_value = LEFT_CHILD;
        }
    }

    if (parent->left == leaf) child_value = LEFT_CHILD;
    else child_value = RIGHT_CHILD;

    if (parent->color == BLACK) return;
    else {
        // parent color is red
        // find out the color of uncle
        if (uncle != NULL){
            if (uncle->color == RED){
                parent->color = BLACK;
                uncle->color = BLACK;
                if (grandParent->parent != NULL){
                    grandParent->color = RED;
                    return bstree_fixup(grandParent);
                }
            }
            // move the problem above
        }else {
            // rotate when uncle is Black
            if (child_value == LEFT_CHILD){
                if (parent_value == LEFT_CHILD){
                    right_rotate(grandParent, parent);
                    swap_color(grandParent, parent);
                }else {
                    right_rotate(parent, leaf);
                    left_rotate(grandParent, leaf);
                }
            }else {
                if (parent_value == LEFT_CHILD){
                    left_rotate(parent, leaf);
                    right_rotate(grandParent, leaf);
                }else {
                    left_rotate(grandParent, parent);
                    swap_color(grandParent, parent);
                }
            }
        }
    }
}

void inorder(node * head){
    if (head != NULL){
        inorder(head->left);
        printf("%d ", head->data);
        if (head->color == RED) printf ("RED\n");
        else printf ("BLACK\n");
        inorder(head->right);
    }
}

void create (node **HEAD){
    char c;
    do{
        node *tmp = (node *)malloc(sizeof(node));
        scanf("%d%c", &tmp->data, &c);
        n ++;
        if (n == 1){
            *HEAD = tmp;
            tmp->parent = NULL;
        }else {
            node * parent = insert_bstree(*HEAD, tmp->data);
            if (parent->data < tmp->data) parent->right = tmp;
            else parent->left = tmp;
            tmp->parent = parent;
        }
        tmp->left = NULL;
        tmp->right = NULL;
        tmp->color = RED; // making all leaf nodes red
        bstree_fixup(tmp);
        inorder(*HEAD);
        printf("\n\n");
        if (c == '\n') return;
    }while(1);
}

int main(){
    node * HEAD = NULL;    
    create (&HEAD);
}
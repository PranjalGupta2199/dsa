#include <stdio.h>
int min = 100000;

void shortest_path(int n, int visit[n], int a[n][n], int u, int v, int path){
    if (a[u][v] != 0) {
        if (path + a[u][v] < min) min = path + a[u][v];
        return;
    }
    visit[u] = 1;
    for (int i = 0; i < n; i++){
        if (visit[i] != 1 && a[u][i] != 0){
            shortest_path(n,visit,a,i,v, path + a[u][i]);
        }
    }
    visit[u] = 0;
} 

int main(){
    int n,m;
    scanf("%d %d", &n, &m);

    int a[n][n];
    int visit[n];
    for (int i =0; i < n; i++){
        visit[i] = 0;
        for (int j = 0; j  <n; j++) a[i][j] = 0;
    } 
    
    int u,v;
    for (int i = 0; i < m; i++){
        scanf("%d %d", &u, &v);
        a[u][v] = 1;
        a[v][u] = 1;
    }   

    scanf("%d %d", &u, &v);
    shortest_path(n,visit,a,u,v,0);
    printf("%d\n", min);
}

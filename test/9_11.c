#include <stdio.h>
#include <stdlib.h>
int currIndex = 0;

typedef struct node{
    struct node *left, *right;
    int data;
}node;

int find(int n, int pre[n], int value){
    for (int i = 0; i < n; i++){
        if (pre[i] == value) return i;
    }
}

node * build_tree(int n, int in[n], int pre[n], int start, int end){
    if (start > end) return NULL;

    node * tmp = (node *)malloc(sizeof(node));
    tmp->data = pre[currIndex++];

    if (start == end) return tmp; // leaf node
    int index = find(n,in,tmp->data);

    tmp->left = build_tree(n,in,pre,start,index-1);
    tmp->right = build_tree(n,in,pre,index+1,end);
}

void postorder(node *HEAD){
    if (HEAD == NULL) return;
    else {
        postorder(HEAD->left);
        postorder(HEAD->right);
        printf("%d ", HEAD->data);
    }
}

int main(){
    int n;
    scanf("%d", &n);

    int in[n], pre[n];
    for (int i = 0; i <n; i++) scanf("%d", &in[i]);
    for (int i = 0; i <n; i++) scanf("%d", &pre[i]);

    node * HEAD = build_tree(n,in,pre,0,n-1);
    postorder(HEAD);
}
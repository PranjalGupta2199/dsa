#include <stdio.h>
int counter = 0;
int curr = 0;

void bfs(int n, int a[n][n], int visit[n], int queue[],int index){
    if (visit[index] == 1) return;
 
    visit[index] = 1;
    printf("%d ", index);
    for (int i = 0; i < n; i++){
        if (visit[i] != 1 && a[index][i] == 1){
            queue[counter++] = i;
        }
    }
}

void call_bfs(int n, int a[n][n], int visit[n], int queue[],int index){
    while (curr != counter){
        bfs(n,a,visit,queue,queue[curr]);
        curr++;
    }
}

void dfs (int n, int a[n][n], int visit[n], int index){
    if (visit[index] == 1) return;
    visit[index] = 1;

    printf("%d ", index);
    for (int i = 0; i < n; i++){
        if (a[index][i] == 1) dfs(n,a,visit,i);
    }
}

int main(){
    int n;
    scanf("%d", &n);

    int a[n][n];
    int visit[n];
    int queue[n*(n-1)/2];
    
    for (int i = 0; i < n; i++) {
        visit[i] = 0;
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }

    int m;
    scanf("%d", &m);

    for (int i =0; i < m; i++) {
        int u,v;
        scanf("%d %d", &u, &v);
        a[u][v] = 1;
        a[v][u] = 1;
    }
    /*
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (a[i][j] == 1){
                bfs(n,a,visit,queue,i);
                call_bfs(n,a,visit,queue,i);
            }
        }
    }*/
    dfs(n,a,visit,0);
}
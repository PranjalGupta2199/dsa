#include <stdio.h>
#include <stdlib.h>

int currIndex = 0;

typedef struct node {
    struct node *left, *right;
    int data;
}node;

int find(int n, int in[n], int value){
    for (int i =0 ; i < n; i++){
        if (in[i] == value) return i;
    }
}

node * build_tree(int n, int in[n], int post[n], int start, int end){
    if(start > end) return NULL;

    node * tmp = (node *)malloc(sizeof(node));
    tmp->data = post[currIndex--];

    if (start == end) return tmp; // leaf node
    int index = find(n,in,tmp->data);

    tmp->right = build_tree(n,in,post,index+1,end);
    tmp->left = build_tree(n,in,post,start,index-1);
}

void preorder(node *HEAD){
    if (HEAD == NULL) return;
    else {
        printf("%d ", HEAD->data);
        preorder(HEAD->left);
        preorder(HEAD->right);
    }
}

int main(){
    int n;
    scanf("%d", &n);

    int in[n], post[n];
    for (int i =0; i < n; i++) scanf("%d", &in[i]);
    for (int i =0; i < n; i++) scanf("%d", &post[i]);
    currIndex = n-1;

    node *HEAD = build_tree(n,in,post,0,n-1);
    preorder(HEAD);
}
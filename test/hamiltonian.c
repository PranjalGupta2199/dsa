#include <stdio.h>
#include <stdlib.h>

void dfs (int n, int a[][n], int visit[n], int index){
    if (visit[index] == 1) return;
    visit[index] = 1;

    for (int i = 0; i < n; i++){
        if (a[index][i] == 1) dfs(n,a,visit,i);
    }
    visit[index] = 0;
}

int sum(int n, int a[n]){
    int temp = 0;
    for (int i = 0; i < n; i++) temp += a[i];
    return temp;
}

void clear (int n, int visit[n]){
    for (int i = 0; i < n; i++) visit[i] = 0;
}
int main(){
    int n,m;
    scanf("%d %d", &n, &m);

    int a[n][n];
    int visit[n];
    for (int i =0; i < n; i++){
        visit[i] = 0;
        for (int j = 0; j  <n; j++) a[i][j] = 0;
    } 
    
    int u,v;
    for (int i = 0; i < m; i++){
        scanf("%d %d", &u, &v);
        a[u][v] = 1;
        a[v][u] = 1;
    } 
    
    for (int i = 0; i < n; i++){
        clear(n,visit);
        dfs(n,a,visit,i);
        if (sum(n,visit) == n) {
            printf("YES\n");
            exit(0);
        }
            
    }
    printf("NO\n");
    
}
#include <stdio.h>

void swap (int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void sort(int * a, int * b, int * c, int m){
    for (int i = 0 ; i < m; i++){
        for (int j = i; j < m; j++){
            if (c[i] > c[j]){
                swap(&a[i], &a[j]);
                swap(&b[i], &b[j]);
                swap(&c[i], &c[j]);
            }
        }
    }
}

int main()
{
    int n, m;
    scanf ("%d", &n);
    scanf("%d", &m);

    int visited[n];
    int c[m];
    int a[m];
    int b[m];

    int ans = 0;

    for (int i = 0; i < n; i++){
        visited[i] = i;
    }
    for (int i = 0; i < m; i++){
        a[i] = 0;
        b[i] = 0;
        c[i] = 0;
    }

    for (int i = 0; i < m; i++){
        scanf("%d %d %d", &a[i], &b[i], &c[i]);
    }

    for (int i = 0; i < m; i++){
        a[i] --;
        b[i] --;
    }

    sort (a,b,c,m);

    for (int i = 0; i < m; i++){
     
        if (visited[a[i]] != visited[b[i]]){
            ans += c[i];

            int e = visited[b[i]];
            for (int j = 0; j < n; j++)
            {
                
                if (visited[j] == e)
                {
                    visited[j] = visited[a[i]];
                }
            }
        }

    }

    printf("%d\n", ans);
}

#include <stdio.h>
void swap(int *a, int u, int v){
    int temp = a[u];
    a[u] = a[v];
    a[v] = temp;
}

int partition(int *a, int low, int high){
    int left = low;
    int pivot = low;
    int right = high;

    while (left < right){
        while (a[left] <= a[pivot]) left ++;
        while (a[pivot] < a[right]) right --;
        
        if (left < right) swap(a,left, right);
    }

    swap(a,right, pivot);
    return right;
}

void quicksort(int *a, int low, int high){
    if (low < high){
        int pivot = partition(a,low,high);
        quicksort(a, low, pivot - 1);
        quicksort(a, pivot + 1, high);
    }
}

int main(){
    int n;
    scanf("%d", &n);
    
    int a[n];
    for (int i = 0; i < n; i++) scanf("%d", &a[i]);
    
    quicksort(a,0,n - 1);

    for (int i = 0; i < n; i++) printf("%d ", a[i]);
}
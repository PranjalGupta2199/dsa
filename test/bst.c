#include <stdio.h>
#include <stdlib.h>

typedef struct node{
    struct node *left, *right;
    int data;
}node;

node * binary_search(node *HEAD_REF, int value){
    node *HEAD = HEAD_REF;
    while (HEAD->left != NULL || HEAD->right != NULL){
        if (value <= HEAD->data){
            if (HEAD->left == NULL) return HEAD;
            else HEAD = HEAD->left;
        }
        else {
            if (HEAD->right == NULL) return HEAD;
            else HEAD = HEAD->right;
        }
    }
    return HEAD;
}

void add(node **HEAD){
    int n;
    scanf("%d", &n);

    int t;
    for (int i = 0; i < n; i++){
        scanf("%d", &t);
        node *tmp = (node *)malloc(sizeof(node));
        if (i == 0){
            tmp->data = t;
            *HEAD = tmp;
        }
        else {
            node *temp = binary_search(*HEAD, t);
            tmp->data = t;
            if (t <= temp->data){
                temp->left = tmp;
            }
            else {
                temp->right = tmp;
            }   
        }
    }
}

void inorder(node *HEAD){
    if (HEAD == NULL) return; 
    else{
        inorder(HEAD->left);
        printf("%d ", HEAD->data);
        inorder(HEAD->right);
    }
    //else printf("%d ", HEAD->data);
}

void preorder(node *HEAD){
    if (HEAD == NULL) return;
    else {
        printf("%d ", HEAD->data);
        preorder(HEAD->left);
        preorder(HEAD->right);
    }
}

void postorder(node *HEAD){
    if (HEAD == NULL) return;
    else {
        postorder(HEAD->left);
        postorder(HEAD->right);
        printf("%d ", HEAD->data);
    }
}

int main(){
    node *HEAD = NULL;
    add(&HEAD);
    inorder(HEAD);
    printf("\n");
    preorder(HEAD);
    printf("\n");
    postorder(HEAD);
}
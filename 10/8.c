#include <stdio.h>
int space = 0;
int l = 0;
int r = 0;

void find(int n,int a[n]){
    for (int left = 0; left < n; left++){
        for (int right = n-1; right >= 0; right --){
            int count = 0;
            int flag = 0;
            for (int i = left; i <= right; i++){
                if (a[i] != -1) {
                    flag = 1;
                    break;
                }
                else count ++;
            }
            if (flag != 1 && count > space) {
                space = count;
                l = left;
                r = right;
            }
            else if (flag != 1 && count == space && left < l){
                l = left;
                r = right;
            }
        }
    }
}

int main(){
    int n, k;
    scanf("%d %d", &n, &k);

    int a[n];
    for (int i = 0; i < n; i++) a[i] = -1;

    char s[k];
    scanf("%s", s);

    int Q;
    scanf("%d", &Q);

    for (int i = 0; i < k; i++){
        space = 0;
        find(n,a);
        int mid = l + (r-l+1)/2;
        if ((r-l+1)%2 == 0){
            if (s[i] == 'L'){
                a[mid - 1] = i+1;
            }
            else {
                a[mid] = i+1;
            }
        }
        else {
            a[mid] = i+1;
        }
    }
    
    int v;
    for (int q = 0; q < Q; q++){
        scanf("%d", &v);
        printf("%d\n", a[v-1]);
    }
}
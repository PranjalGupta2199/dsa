#include <stdio.h>
#define MAX 10000

int n = 0;

void add(int a[MAX]){
    char c;
    do{
        scanf("%d%c", &a[n],&c);
        n ++;
        if (c == '\n') return;
    }
    while (1);
}

int main(){
    int a[MAX];
    add(a);
    int ans = 0;

    for (int left = 0; left < n; left++){
        for (int right = n - 1; right >= 0; right --){
            int count_0 = 0;
            int count_1 = 0;
            for (int i = left; i <= right; i++){
                if (a[i] == 0) count_0 ++;
                else count_1 ++;
            }
            if (count_0 == count_1 && count_0 != 0 && count_1 != 0) ans ++;
        }
    }
    printf("%d", ans);
}
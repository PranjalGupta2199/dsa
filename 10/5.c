#include <stdio.h>

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void max_heapify(int n, int a[n], int index){
    int left = 2*index + 1;
    int right = 2*index + 2;
    int largest = index;

    if (left < n && a[left] > a[largest]) largest = left;
    if (right < n && a[right] > a[largest]) largest = right;
    if (largest != index){
        swap(&a[largest], &a[index]);
        max_heapify(n,a,largest);
    }
}

int main(){
    int n;
    scanf("%d", &n);

    int min[n];
    for (int i = 0; i < n; i++) scanf("%d", &min[i]);

    for (int i = n; i >= 0; i--) max_heapify(n, min, i);
    for (int i = 0; i < n; i++) printf("%d ", min[i]);
}

#include <stdio.h>
#include <stdlib.h>

void swap(int *a, int *b){
    int temp = *a;
    *a = *b; 
    *b = temp;
}

void min_heapify(int n, int a[n], int index){
    int left = 2*index;
    int right = 2*index + 1;
    int smallest = index;

    if (left < n && a[left] < a[index]) smallest = left;
    if (right < n && a[right] < a[smallest]) smallest = right;

    if (smallest != index) {
        printf("False\n");
        exit(0);
    }
}

int main(){
    int n;
    scanf("%d", &n);

    int a[n+1];
    for (int i = 1; i < n; i++) scanf("%d", &a[i]);

    for (int i = 1; i < n; i++) min_heapify(n,a,i);
    printf("True\n");
}
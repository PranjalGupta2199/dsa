#include <stdio.h>

void swap(int *a, 
int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void max_heapify(int n, int a[n], int index){
    int left = 2*index + 1;
    int right = 2*index + 2;
    int largest = index;

    if (left < n && a[left] > a[index]) largest = left;
    else if (right < n && a[right] > a[largest]) largest = right;

    if (largest != index){
        swap(&a[largest], &a[index]);
        max_heapify(n,a,largest);
    }
}

void delete(int n, int a[n]){
    int temp = a[0];
    a[0] = a[n-1];
    max_heapify(n-1,a,0);
    a[n-1] = temp/2;
    max_heapify(n,a,0);
}

int main(){
    int n, k;
    scanf("%d %d", &n, &k);

    int a[n];
    for (int i = 0; i < n; i++) scanf("%d", &a[i]);

    for (int i = n/2; i >= 0; i--) max_heapify(n,a,i);
    int ans = 0;

    for (int i = 0; i < n; i++) printf("%d ", a[i]);

    while (k != 0){
        ans += a[0];
        delete(n,a);
        k --;
    }
    printf("%d", ans);
}
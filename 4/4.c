#include <stdio.h>
#include <math.h>
#include <limits.h>

int min = INT_MAX;
int t = 0;
int h = 0;

int power(int x, int n){
	if (x == 0) return 1;
	else if (x == 1) return n;
	else {
		int p = 1;
		for (int i = 0; i < n; i++) p *= x;
		return p;
	}
}

int main(){
	int n;
	scanf("%d", &n);

	int sum = 0;
	int tower[n];
	for (int i = 0; i < n; i++) {
		scanf("%d", &tower[i]);
		sum += tower[i];
	}
	int cost[n];
	for (int i = 0; i < n; i++) scanf("%d", &cost[i]);

	int select[n];
	for (int i = 0; i < n; i++) select[i] = 0;


	int newsum = 0, count = 0;
	for (int i = 1; i < power(2,n); i++){
		count = 0;
		newsum = 0;
		for (int j = n; j >= 1; j--){
			if ((power(2,n-j)&i) == power(2,n-j)){
				select[j-1] = 1;
				count ++;
			}
			else {
				select[j-1] = 0;
			}
		}

		if (count != 0){
			int div  = sum/count;
			
			if (sum%count == 0 && count <= n){
				for (int j = 0; j < n; j++){
					if (select[j] == 0) newsum += tower[j]*cost[j];
					else newsum += fabs(div - tower[j])*cost[j];
				}

				if (newsum < min){
					min = newsum;
					t = count;
					h = div;
				}
				for (int j =0; j < n; j++) select[j] = 0;
			}

			
		}

	}

	printf("The min cost %d\n", min);
	printf("The min ht %d\n", h);
	printf("The number of towers %d\n", t);
}
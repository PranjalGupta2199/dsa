#include <stdio.h>
#include <limits.h>

int MAX = INT_MIN;
int ITEMS = 0;

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void sort(int n, int a[]){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (a[i] < a[j]) swap(&a[i], &a[j]);
        }
    }
}

void calculate(int n, int a[n], int k, int s){
    int cost [n];

    for (int i = 0; i < n; i++) cost[i] = a[i] + (i+1)*k;
    sort(n,cost);
    
    int ans = 0;
    for (int i = 0; i < k; i++) ans += cost[i];
    if (ans > MAX && ans <= s) {
        MAX = ans;
        ITEMS = k;
    }
}

int main(){
    int n,s;
    scanf("%d %d", &n, &s);

    int cost[n];
    for (int i = 0; i < n; i++) scanf("%d", &cost[i]);

    for (int i = n-1; i >= 0; i--) calculate (n,cost, i+1,s);
    printf("%d %d\n", ITEMS, MAX);
}
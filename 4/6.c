#include <stdio.h>
#include <math.h>
#include <stdlib.h>

int main(){
	int n;
	scanf("%d", &n);

	int a[n];
	for (int i = 0; i < n; i++) scanf("%d", &a[i]);

	int d;
	scanf("%d", &d);

	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			if (fabs(a[i] - a[j]) == d) {
				printf("%d\n", a[i] + a[j]);
				exit(0);
			}
		}
	}
	printf("-1");

}
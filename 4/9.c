#include <stdio.h>

void binarySearch(int n, int a[n], int k){
    if (k > a[n-1]) {
        printf("Sasuke %d\n", n+1);
        return;
    }
    int f = 0;
    int l = n - 1;
    int m = (f + l)/2;

    while (a[m] != k){
        if (k > a[m]) f = m;
        else if (k < a[m]) l = m;

        if (m == (f+l)/2) break;
        else m = (f+l)/2;
    }
    if (a[m] == k) printf("Naruto %d\n", m+1);
    else printf("Sasuke %d\n", l+1);
}

int main(){
    int n;
    scanf("%d", &n);

    int a[n];
    for (int i = 0; i < n; i++) scanf("%d", &a[i]);

    int k;
    scanf("%d", &k);

    binarySearch(n,a,k);
}
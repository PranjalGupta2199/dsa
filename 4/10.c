#include <stdio.h>
int N = 100000;

int main(){
    int n;
    scanf("%d", &n);

    int index = 0;
    int count[N];
    for (int i = 0; i < N; i++) count[i] = 0;
    
    int a[n];
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
        count[a[i]- 1] += 1;
    }

    for (int i = 0; i < N; i++) {
        if (count[i] %2 == 1) {
            printf("%d\n", i+1);
            break;
        }
    }
}
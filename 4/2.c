#include <stdio.h>
#include <stdlib.h>

void calculate(int V, int k, int n){
	int count = V;
	int v = V;
	while (v != 0){
		v /= k;
		count += v;
	}
	if (count >= n) {
		printf("%d\n", V);
		exit(0);
	}
}

int main(){
	int n,k;
	scanf("%d %d", &n, &k);

	int ans = k;
	while (1) {
		calculate (ans, k, n);
		ans ++;
	}
}
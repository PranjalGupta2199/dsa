#include <stdio.h>
#include <limits.h>

int binarySearch(int n, int a[n], int f, int l, int k){
	int m = (f+l)/2;

	while (f <= l){
		m = (f+l)/2;
		if (a[m] == k) return m;
		else if (a[m] > k) l = m - 1;
		else f = m + 1;
	}
	return - 1;
}

int main(){
	int n,k;
	scanf("%d", &n);

	int a[n];

	int min = INT_MAX;
	int max = INT_MIN;
	int minI = 0;
	int maxI = 0;

	for (int i = 0; i < n; i++) {
		scanf("%d", &a[i]);
		if (a[i] > max) {
			max = a[i];
			maxI = i;
		}
		else if (a[i] < min){
			min = a[i];
			minI = i;
		}
	}
	scanf("%d", &k);

	if (min != 0) {
		int c = binarySearch(n,a,minI,n-1,k);
		if ( c == -1){
			printf("%d\n", binarySearch(n,a,0,maxI,k));
		}
		else printf("%d\n",c + 1);
	}
	else {
		int c = binarySearch(n,a,0,n-1,k);
		if (c == -1) printf("-1\n"); 
		else printf("%d\n",c+1);
	}
}
#include <stdio.h>
int l,r;

int bsearch(int n, int *a, int k){
    int f = 0;
    int l = n - 1;
    int m;

    while (f <= l){
        m = (f+l)/2;
        if (a[m] == k) return m;
        else if (a[m] > k) l = m - 1; 
        else f = m + 1;
    } 
    return -1;
}

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

int main(){
    int n;
    scanf("%d", &n);

    int a[n];
    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
        if (i != 0 && a[i-1] > a[i]) {
            swap(&a[i], &a[i-1]);
            l = i-1;
            r = i;
        }
    }
    int k;
    scanf("%d", &k);

    int ans = bsearch(n,a,k);
    if (ans != -1){
        if (ans == l) ans ++;
        else if (ans == r) ans --;
        printf("%d\n",ans+1);
    }
    else printf("%d\n",-1);
}
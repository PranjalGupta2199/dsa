#include <stdio.h>
#include <limits.h>

void swap(int * a, int *b){
    int tmp = *b;
    *b = *a;
    *a = tmp;
}

int digit(int n){
    int count = 0;

    while (n != 0){
        count ++;
        n /= 10;
    }
    return count;
}

int power(int n, int d){
    int p = 1;
    for (int i = 0; i < d; i++) p *= n;
    return p;
}

int compare(int u, int v){
    int d_u = digit(u);
    int d_v = digit(v);

    int u1 = u*power(10,d_v) + v;
    int v1 = v*power(10,d_u) + u;

    if (u1 < v1) return -1;
    else if (u1 == v1 ) return 0;
    else return 1;
}



int main(){
    int n;
    scanf("%d", &n);

    int a[n];
    for (int i = 0; i < n; i++) scanf("%d", &a[i]);

    for (int i = 0; i < n; i++) {
        for (int j = i; j < n; j++){
            if (compare(a[i], a[j]) == -1) swap(&a[i], &a[j]);
        }
    }

    for (int i = 0; i < n; i++) printf("%d", a[i]);
    printf("\n");
}
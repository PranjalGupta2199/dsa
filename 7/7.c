#include <stdio.h>

int n = 0;

void add (int a[1000]){
    char c;
    do{
        scanf("%d%c", &a[n], &c);
        n ++;
        if (c == '\n') return;
    }
    while (1);
}

int main(){
    int a[1000];
    add(a);

    int count = 0;
    for (int i = 0; i < n; i++){
        for (int j = 0; j < i; j++){
            if (a[j] > a[i]) count ++;
        }
        for (int j = i+1; j < n; j++){
            if (a[j] < a[i]) count ++;
        }
    }
    printf("%d\n", count/2);

}
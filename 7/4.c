#include <stdio.h>

void swap(int *a, int *b){
    int temp = *b;
    *b = *a;
    *a = temp;
}

void sort(int n, int a[n]){
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++){
            if (a[i] > a[j]) swap(&a[i], &a[j]);
        }
    }
}

int main(){
    int n,m;
    scanf("%d %d", &n, &m);

    int a[n];

    int count[m+1];
    for (int i = 0; i < m+1; i++) count[i] = 0;

    for (int i = 0; i < n; i++) {
        scanf("%d", &a[i]);
        count[a[i]] ++;
    }

    sort(n,a);

    for (int i = 0; i < n; i++) printf("%d\n",a[i] );

    int ans = 0;

    for (int i = 0; i < n - 1; i++){
        if (count[a[i]] > 1){
            int v = a[i] - 1;
            for (int j = 1; j < count[a[i]]; j++){
                a[i+j] = v; 
                ans += a[i+j] - v;
                if (v != 1) v--;
                
            }
            i = i + count[a[i]] - 1;
            if( v== 1){
                for (int j = i; j < n; j++) ans += a[j] - 1;
                    i = n -1;
            }
        }
        else {
            printf("HI\n");
            if (a[i] != 1) ans += a[i + 1];
        }
    }
    printf("%d\n", ans);
}
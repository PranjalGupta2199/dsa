#include <stdio.h>

int path = 0;

void dfs(int n, int a[n][n], int visit[n], int u, int v, int l){
    if (a[u][v] == 1) path = l + 1;
    else {
        visit[u] = 1;
        for (int i = 0; i < n; i++) {
            if (a[u][i] == 1 && visit[i] != 1){
                dfs(n,a,visit,i,v,l+1);
            }
        }
    }
}

void clear (int n, int a[n]){
    for (int i = 0; i < n; i++) a[i] = 0;
}

int main(){
    int n,x;
    scanf("%d %d", &n, &x);

    x--;
    int d_x[n], d_0[n],a[n][n],visit[n];

    for (int i = 0; i < n; i++) {
        visit[i] = 0;
        d_x[i] = 0;
        d_0[i] = 0;
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }

    for (int i = 0; i < n - 1; i++) {
        int u,v;
        scanf("%d %d", &u, &v);
        a[u-1][v-1] = 1;
        a[v-1][u-1] = 1;
    }


    for (int i = 0; i < n; i++) {
        path = 0;
        clear(n,visit);
        if (i != x) dfs(n,a,visit,x,i,0);
        d_x[i] = path;
    }

    for (int i = 0; i < n; i++) {
        path = 0;
        clear(n,visit);
        if (i != 0) dfs(n,a,visit,0,i,0);
        d_0[i] = path;
    }    

    int max = -1;
    for (int i = 0; i < n; i++){
        if (d_0[i] > d_x[i]){
            if (2*d_0[i] > max) max = 2*d_0[i];
        }
    }
    printf("%d\n", max);
}
#include <stdio.h>
int s_t = 0;

void shortest_path(int n, int a[n][n], int visit[n], int u, int v, int path){
    if (a[u][v] == 1) {
        if (path+1 < s_t) s_t = path + 1;
        return;
    }
    else {
        visit[u] = 1;
        for (int i = 0; i < n; i++){
            if (a[u][i] == 1 && visit[i] != 1) {
                shortest_path(n,a,visit,i,v,path+1);
                visit[i] = 0;
            }
        }
    }
}

void clear (int n, int array[n]){
    for (int i = 0; i < n; i++) array[i] = 0;
}

int main(){
    int n,m,s,t;
    scanf("%d %d %d %d", &n, &m, &s, &t);
    s--;
    t--;
    s_t = n;

    int a[n][n], visit[n];

    for (int i = 0; i < n; i++){
        visit[i] = 0;
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }

    int u,v;
    for (int i = 0; i < m; i++) {
        scanf("%d %d", &u, &v);
        a[u-1][v-1] = 1;
        a[v-1][u-1] = 1;
    }

    shortest_path(n,a,visit,s,t,0);
    int path = s_t;
    int count = 0;

    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) {
            s_t = n;
            if (a[i][j] == 0 && i != j){
                a[i][j] = 1;
                a[j][i] = 1;
                clear(n,visit);
                shortest_path(n,a,visit,s,t,0);
                a[i][j] = 0;
                a[j][i] = 0;

                if (s_t == path) count ++;
            }
        }
    }
    printf("%d\n", count/2);

}
#include <stdio.h>
#include <math.h>

int digit(int n){
    int count = 0;

    while (n != 0){
        count ++;
        n /= 10;
    }
    return count;
}

int power(int n, int d){
    int p = 1;
    for (int i = 0; i < d; i++) p *= n;
    return p;
}

int main(){
    int n;
    scanf("%d", &n);

    for (int i = 0; i < n; i++) {
        if (i < 10) printf("%d ", i);
        else {
            int d_i = digit(i) - 1;
            int flag = 0;
            int v = i;
            int a = v%10;
            v /= 10;

            while (d_i != 0){
                int b = v%10;
                if (fabs(a-b) != 1){
                    flag = 1;
                    break;
                }
                a = b;
                v/= 10;
                d_i --;
            }

            if (flag == 0) printf("%d ", i);
        }
    }
}
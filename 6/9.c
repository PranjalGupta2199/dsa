#include <stdio.h>

int main(){
    int n;
    scanf("%d", &n);

    int c = 0;

    if (n >= 75) {
        n = 0;
        c = -1;
    }

    while (n != 0){ 
        if (n <= 3) {
            c ++;
            n --;
        }
        else if (c+3 <= c*2 && c != 0){
            int tmp = c;
            n -= 3;
            c += tmp;
            c += tmp*n;
            n = 0;
        }
        else {
            c ++;
            n --;
        }
    }
    printf("%d\n",c);
    
}
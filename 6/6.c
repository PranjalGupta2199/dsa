#include <stdio.h>
#include <string.h>
#include <limits.h>

int palindrome(char a[1000], int i, int j){
    int k = (j-i)/2 + i; 
    if (j - i + 1 == 2) return 0;
    int temp = i;
    for (int r = i, c = j; r <= c; r++,c--){
        if (a[r] != a[c]) return 0;
        else {
            if (r <= k && a[temp] <= a[r]){
                temp = r;
            }
            else return 0;
        }
    }
    return 1;
}

int main(){
    char s[1000];
    scanf("%s", s);

    int n = strlen(s);
    
    int pos[1000][2];
    int counter = 0;


    int max = INT_MIN;
    int r = 0;
    int c = 0;
    
    for (int i = 0; i < n; i++){
        for (int j = n -1; j > i; j--){
            if (palindrome(s,i,j) == 1 ){
                if (j - i + 1 > max){
                    max = j - i + 1;
                    r = i;
                    c = j;
                } 
                else if (j - i +1 == max && s[r] > s[i]){
                        r = i;
                        c = j;
                }
            }
            else {}
        }
    }
    if (max == INT_MIN) printf("-1\n");
    else {
        for (int i = r; i <= c; i++) printf("%c", s[i]);
        printf("\n");
    }
}
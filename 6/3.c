#include <stdio.h>
int m;
int ans = 0;

void path(int n, int g[n][n], int visit[n], int v[n], int r, int counter, int flag){
    if (counter > m) return;
    visit[r] = 1;

    for (int i = 0; i < n; i++){
        if (g[r][i] == 1 && visit[i] != 1){
            flag = 1;
            if (v[i] == 0) path(n,g,visit,v,i,0,0);
            else {
                path(n,g,visit,v,i,counter + 1,0);
            }
        }
    }
    if (flag == 0 && counter <= m) ans ++;
    return;
}


int main(){
    int n;
    scanf("%d %d", &n, &m);

    int v[n];
    for (int i = 0; i < n; i++) scanf("%d", &v[i]);

    int g[n][n];    
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) g[i][j] = 0;
    }
    for (int i = 0; i < n-1; i++){
        int a,b;
        scanf("%d %d", &a, &b);
        g[a-1][b-1] = 1;
        g[b-1][a-1] = 1;
    }


    int visit[n];
    for (int i = 0; i < n; i++) visit[i] = 0;
    path(n,g,visit,v,0,v[0],0);
    printf("%d\n",ans );
}
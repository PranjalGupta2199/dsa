#include <stdio.h>
int temp = 0;

int sort (int n, int array[n]){
    int flag = 0;
    for (int i = 1; i < n; i++) {
        if (array[temp] <= array[i]) {
            flag = 0;
            temp = i;
        }
        else return 0;
    }
    return 1;
}

void swap(int *a, int *b){
    int tmp = *b;
    *b = *a;
    *a = tmp;
}


int find_min(int n, int m, int a[n][m],int row, int index){
    int min = index;
    for (int i = index; i < m; i++){
        if (a[row][i] < a[row][min]) min = i;
    }
    return min;
}

int main(){
    int n,m;
    scanf("%d %d", &n, &m);

    int a[n][m];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++) scanf("%d", &a[i][j]);
    }

    int I[m*(m-1)/2][2];

    int counter = 0;
    for (int i = 0; i < n; ){
        temp = 0;
        if (sort (m,&a[i][0]) == 1) i++;
        else {
            int index = find_min(n,m,a,i,temp);
            int flag = 0;
            for (int j = 0; j < n; j++){
                if (a[j][temp] > a[j][index]) {
                    swap(&a[j][index], &a[j][temp]);
                    flag = 1;
                }
            }
            if (flag == 1) {
                I[counter][0] = temp + 1;
                I[counter][1] = index + 1;
                counter ++;
            }

        }
    }
    printf("\n%d\n", counter);

    for (int i = 0; i < counter; i++) printf("%d %d\n", I[i][0], I[i][1]);
}
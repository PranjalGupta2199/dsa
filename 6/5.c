#include <stdio.h>
#include <stdlib.h>

typedef struct node{
    int n;
    struct node * next;
}node;


void create(node ** head, int n){
    node *head_ref = *head;
    for (int i = 0; i < n; i++){
        node * tmp = (node *)malloc(sizeof(node));
        scanf("%d", &(tmp->n));

        if (i == 0) {
            *head = tmp;
        }
        else head_ref ->next = tmp;
        head_ref = tmp;

    }
}


node * merge_sort(node * result, node *head1, node *head2){
    if (head1 == NULL) return head2;
    else if (head2 == NULL) return head1;
    else if (head1->n <= head2->n){
        result = head1;
        result->next = merge_sort(result->next, head1->next, head2);
    }
    else {
        result = head2;
        result->next = merge_sort(result->next, head1, head2->next);
    }
    return result;
}


void printList(node *head){
    while(head != NULL) {
        printf("%d ", head->n);
        head = head->next;
    }
}

int main(){
    int n;
    node *head1 = NULL;
    
    scanf("%d", &n);
    create(&head1, n);

    int m;
    scanf("%d", &m);

    node *head2 = NULL;
    create(&head2, m);

    node * merge_head = NULL;
    merge_sort(merge_head, head1, head2);

    printList(head1);
    //printList(head2);
    //printList(merge_head);



}
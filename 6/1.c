#include <stdio.h>
void swap(int *a, int *b){
    int temp = *b;
    *b = *a;
    *a = temp;
}

int main(){
    int n;
    scanf("%d", &n);

    int a[n];
    for (int i = 0; i < n; i++ )scanf("%d", &a[i]);

    int b[n][n];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) scanf("%d", &b[i][j]);
    }

    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (b[i][j] == 1 && a[i] < a[j]) {
                swap(&a[i], &a[j]);
            }
        }
    }
    for (int i = 0; i < n; i++) printf("%d ", a[i]);
    printf("\n");
}
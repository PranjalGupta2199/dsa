#include <stdio.h>
#include <limits.h>

int find_min(int n, int a[n], int index){
	int min = INT_MAX;
	int j = 0;

	for (int i = index; i < n; i++) {
		if (a[i] < min) {
			min = a[i];
			j = i;
		}
	}
	return j;
}

void swap(int *a, int *b){
	int temp = *b;
	*b = *a;
	*a = temp;
}


int selection_sort(int n, int a[n]){
	int e = 0;
	int count = 0;
	for (int i = 0; i < n; i++){
		e = find_min(n,a,i);
		if (a[i] != a[e]) {
			swap(&a[i], &a[e]);
			count ++;
		}
	}
	return count;
}

int main(){
	int n;
	scanf("%d", &n);

	int a[n];
	for (int i = 0; i < n; i++) scanf("%d", &a[i]);

	printf("%d\n", selection_sort(n,a));
}
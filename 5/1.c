#include <stdio.h>
#define MAX 208


int main(){
	int T;
	scanf("%d", &T);

	for (int t= 0; t < T; t++){

		int a[MAX];
		for (int i = 0; i < MAX; i++) a[i] = 0;
		
		int n;
		scanf("%d", &n);

		int u[n], v[n];

		int grp = 1;

		for (int i = 0; i < n; i++) {
			scanf("%d %d", &u[i], &v[i]);
			u[i]--;
			v[i] --;

			if (grp == 1) grp = 2;
			else grp = 1;

			for (int j = u[i]; j <= v[i]; j++){
				if (a[j] == 0) a[j] = grp;
				else if (a[j] != grp){

					if (grp == 1) grp = 2;
					else grp = 1;				
				
					for (int k = u[i]; k <= j; k++) a[k] = grp;
				}	
			}
		}

		int count[3] = {0,0,0};
		for (int i = 0; i < MAX; i++) count[a[i]] ++;
		if (count[1] == 0 || count[2] == 0) printf("-1\n");
		else {
			for (int i = 0; i < n; i++){
				if (a[u[i]] == 1) printf("IM ");
				else printf("CA ");
			}
			printf("\n");
		}
	}
}
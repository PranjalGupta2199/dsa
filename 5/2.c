#include <stdio.h>
#define MAX 107

int main(){
	int n,m;

	scanf("%d %d", &n, &m);
	int a[m];

	int count[MAX];
	for (int i = 0; i < MAX; i++) count[i] = 0;

	int v;
	for (int i = 0; i < n; i++){
		scanf("%d", &v);
		count[v] ++;
	}

	for (int i = 0; i < m; i++) {
		scanf("%d", &a[i]);
	}

	for (int i = 0; i < m; i++){
		if (count[a[i]] != 0) {
			for (int c = 0; c < count[a[i]]; c++) printf("%d ", a[i]);
			count[a[i]] = 0;
		}
	}

	for (int i = 0; i < MAX; i++){
		if (count[i] != 0) {
			for (int c = 0; c < count [i]; c++) printf("%d ", i);
		}
	}
	printf("\n");

}
#include <stdio.h>
int MAX = 100000;
int n = 0;

void add(int a[MAX]){
	char c;
	do{
		scanf("%d%c", &a[n], &c);
		n ++;
		if (c == '\n') return;
	}
	while (1);
}

void insert(int a[MAX], int val){
	if (a[0] >= val){
		for (int i = n; i > 0; i--) a[i] = a[i-1];
		a[0] = val;
		n++;
		return;
	}
	else {
		for (int i = 0; i < n-1; i++){
			if (a[i] <= val && val <= a[i+1]){
				a[n] = a[n-1];
				for (int j = n - 1; j > i; j--) a[j] = a[j-1];
				a[i+1] = val;
				n ++;
				return;
			}
		}
	}
	a[n] = val;
	n++;
	return;
}

void swap(int *a, int *b){
	int temp = *a;
	*a = *b;
	*b = temp;
}

int binarySearch(int a[MAX], int val){
	int f = 0;
	int l = n - 1;
	int m = (f+l)/2;

	while (f <= l){
		m = (f+l)/2;
		if (a[m] == val) return m;
		else if (val > a[m]) f = m +1;
		else if (val < a[m]) l = m - 1;
	}	
	return -1;
}

void delete(int a[MAX], int val){
	int m = binarySearch(a, val);
	if (m == -1 ) return;
	else {
		for (int i = m; i < n-1; i++){
			a[i] = a[i+1];
		}
		a[n-1] = 0;
		n--;
		return;
	}
}

void interchange(int a[MAX], int v1, int v2){
	int m1 = binarySearch(a,v1);
	int m2 = binarySearch(a,v2);
	if (m1 == -1 || m2 == -1) return;
	else swap(&a[m1], &a[m2]);
}

void sort(int a[MAX]){
	for (int i = 0; i < n; i++){
		for (int j = i; j < n; j++) {
			if (a[i] > a[j]) swap(&a[i], &a[j]);
		}
	}
}

void printList(int a[MAX]){
	for (int i = 0; i < n; i++){
		printf("%d ", a[i]);
	}
	printf("\n");
}

void prompt(int a[MAX]){
	char c1,c2;
	int d1,d2;
	scanf("\n%c%c", &c1, &c2);

	if (c1 == 'I') {
		scanf("%d", &d1);
		insert(a,d1);
	}
	else if (c1 == 'D'){
		scanf("%d", &d1);
		delete(a,d1);
	}
	else if (c1 == 'S' && c2 == 'W'){
		scanf("%d %d", &d1, &d2);
		interchange(a,d1,d2);
	}
	else if (c1 == 'S' && c2 == 'O') sort(a);

	printList(a);

}


int main(){
	int a[MAX];
	for (int i = 0; i < MAX; i++) a[i] = 0;

	add (a);
	int t;
	scanf("%d", &t);

	for (int i = 0; i < t; i++){
		prompt(a);
	}
}
#include <stdio.h>

void swap(int *a, int *b){
    int temp = *b;
    *b = *a; 
    *a = temp;
}

void sort (int n, int at[n], int et[n]){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (at[i] < at[j]){
                swap(&at[i], &at[j]);
                swap(&et[i], &et[j]);
            } 
            else if (at[i] == at[j]){
                if (et[i] < et[j]) swap(&et[i], &et[j]);
            }
        }
    }

}

void clear(int n, int q[n][2]){
    for (int i = 0; i < n; i++){
        q[i][0] = 0;
        q[i][1] = 0;
    }

}

int main(){
    int n;
    scanf("%d", &n);
    int threshold;

    int at[n], et[n];

    for (int i = 0; i < n; i++) scanf("%d %d", &at[i], &et[i]);
    scanf("%d", &threshold);

    sort (n,at,et);

    int q[n][2];

    int cur_time = 0;
    int wait[n];
    for (int i = 0; i < n; i++) wait[i] =0 ;

    int endtime[n];
    for (int i = 0; i < n; i++) endtime[i] = 0;
    
    for (int i =0; i < n; i++) {
        int length = 0;
        int j = 0;
        clear(n,q);

        while (cur_time >= at[j] && j < n){
            if (et[j] <= 0) j++;
            else {
                q[length][0] = j;
                q[length][1] = et[j];
                j ++;
                length ++;                
            }
        }

        if (length == 1) break;
        else {
            for (int k = 0; k < length; k++) {
                q[k][1] = et[q[k][0]] - threshold;
                wait[q[k][0]] += cur_time - endtime[q[k][0]];
                if (q[k][1] < 0) cur_time += et[q[k][0]];
                else cur_time += threshold;
                endtime[q[k][0]] = cur_time;
            }

            for (int k = 0; k < length; k++){
                et[q[k][0]] = q[k][1];
            }            
        }
    

    }
    
    for (int i = 0; i < n; i++) printf("%d ", wait[i]);


}
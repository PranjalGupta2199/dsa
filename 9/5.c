#include <stdio.h>
#include <string.h>

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void sort(int n, int a[n]){
    for (int i =0; i <n; i++){
        for (int j = 0; j < n; j++){
            if (a[i] > a[j]) swap(&a[i], &a[j]);
        }
    }
}

int sum(int n, int a[n]){
    int s = 0;
    for (int i = 0; i < n; i++) s+= a[i];
    return s;
}

int main(){
    char s[10000];
    scanf("%s", s);

    int l = strlen(s);

    int count[26];
    int max = 0;
    for (int i = 0; i < 26; i++) count[i] = 0;
    
    for (int i = 0; i < l; i++) count[s[i] - 97] ++;

    sort(26,count);

    while (sum(26, count) >= 2){
        int max = count[0];
        int max2 = count[1];
        if (max2 == 0) break;
        if (max == max2) {
            count[0] = 0;
            count[1] = 0;
        }
        else if (max - max2 == 1){
            count[0] = 1;
            count[1] = 0;
        }
        else if (max > max2){
            count[0] = max-max2;
            count[1] = 0;
        }
        sort(26, count);
    }

    if(sum(26,count) == 0 || sum(26,count) == 1) printf("possible\n");
    else printf("Not possible\n");
}
#include <stdio.h>

int main(){
    int n, m, x;
    scanf("%d %d %d", &n, &m, &x);

    int a[n], b[m];

    for (int i = n-1; i >= 0; i--) scanf("%d", &a[i]);
    for (int i = m-1; i >= 0; i--) scanf("%d", &b[i]);

    int sum = 0;
    int score = 0;

    int top_a = n-1;
    int top_b = m -1;
    while (sum <= x){
        if (top_b == 0 || a[top_a] <= a[top_b]) {
            sum += a[top_a];
            top_a --;
        }
        else {
            sum += b[top_b];
            top_b --;
        }
        score ++;
    }
    printf("%d\n", score);
}
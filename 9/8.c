#include <stdio.h>

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void sort(int n, int p[n], int c[n]){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (p[i] < p[j]) {
                swap(&p[i], &p[j]);
                swap(&c[i], &c[j]);
            }
        }
    }
}

int main(){
    int k,w,n;
    scanf("%d %d %d", &k, &w, &n);

    int p[n], c[n];
    for (int i = 0; i < n; i++) scanf("%d", &p[i]);
    for (int i = 0; i < n; i++) scanf("%d", &c[i]);
 
    sort(n,p,c);  

    int i;
    for (i = 0; i < n; i++){
        if (c[i] <= w) break;
    } 

    w += p[i];
    p[i] = -1;
    c[i] = -1;

    int count = 0;

    while (count != k){
        for (int j = 0; j < n; j++){
            if (c[j] <= w && c[j] != -1) {
                w+= p[j] - c[j];
                c[j] = -1;
                count++;
            }
        }        
    }


    printf("%d\n", w);
}
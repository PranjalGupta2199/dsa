#include <stdio.h>

int main(){
    int n;
    scanf("%d", &n);

    int petrol[n];
    int dist[n];
    int index[n];

    for (int i = 0; i < n; i++) {
        index[i] = i;
        scanf("%d %d", &petrol[i], &dist[i]);
    }

    int min = n;
    for (int i = 0; i < n; i++){
        int flag = 0;
        int p = petrol[i];
        int d = dist[i];
        for (int j = i+1; j < n; j++){
            if (p >= d){
                p -= d;
                p += petrol[j];
                d = dist[j];            
            }
            else {
                flag ++;
                break;
            }
        }

        for (int j = 0; j <= i; j++){
            if (p >= d){
                p -= d;
                p += petrol[j];
                d = dist[j];            
            }
            else {
                flag ++;
                break;
            }
        }
        if (flag == 0) {
            if (i < min) min = i;
        }
    }

    if (min == n) printf("Not possible\n");
    else printf("%d\n", min);
}
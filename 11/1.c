#include <stdio.h>
#include <stdlib.h>

int search(int n, int a[n], int value, int high, int low){
    if (low <= high){
        int mid = (high + low)/2;
        if (a[mid] < value) return search(n,a,value,high,mid+1);
        else if (a[mid] > value) return search(n,a,value,mid-1,low);
        else return mid;
    }
    return -1;
}

void swap(int *a, int *b){
    int temp = *a;
    *a = *b;
    *b = temp;
}

void sort(int n, int a[n]){
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) {
            if (a[i] < a[j]) swap(&a[i], &a[j]);
        }
    }
}

int main(){
    int amt;
    scanf("%d", &amt);

    int n;
    scanf("%d", &n);

    int cost[n];
    for (int i = 0; i < n; i++) scanf("%d", &cost[i]);

    sort(n,cost);
    
    for (int i = 0; i < n; i++){
        int ans = search(n,cost,amt-cost[i],n-1,0);
        if (ans != -1){
            printf("%d %d", cost[i], cost[ans]);
            exit(0);
        }
    }
    printf("-1");
}
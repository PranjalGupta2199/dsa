#include <stdio.h>
#include <string.h>

int main(){
    int m,n;
    scanf("%d %d", &m, &n);

    char mag[m][1000], note[n][1000];
    for (int i = 0; i < m; i++) scanf(" %s", &mag[i][0]);
    for (int i = 0; i < n; i++) scanf(" %s", &note[i][0]);
    
    int count[n];
    for (int i = 0; i < n; i++) count[i]  = 0;
    int ans = 0;

    for (int i = 0; i < n; i++){
        for (int j = 0; j < m; j++){
            if (strcmp(&note[i][0], &mag[j][0]) == 0){
                if (count[i] == 0) {
                    count[i] = 1;
                    ans ++;
                }
            }
        }       
    }
    if(ans == n) printf("Yes\n");
    else printf("No\n");
}
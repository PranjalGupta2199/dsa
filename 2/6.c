#include <stdio.h>
int flag = 0;

void new_graph(int n, int e, int u[e], int v[e], int g[e][e]){
    int e1,e2;
    for (int i = 0; i < e; i++){
        e1 = u[i];
        e2 = v[i];
        for (int j = i + 1; j < e; j++){
            if (u[j] == e2 || v[j] == e2){
                g[i][j] = 1;
                g[j][i] = 1;
            }
        }
        for (int j = i + 1; j < e; j++){
            if (u[j] == e1 || v[j] == e1){
                g[i][j] = 1;
                g[j][i] = 1;
            }
        }
    }
    
}

void check_connectivity(int e, int row, int dsu[e], int g[e][e]){
    for (int i = 0; i < e; i++){
        for (int j = 0; j < e; j++){
            if (i != row && j != row && g[i][j] == 1){
                int e = dsu[j];
                for (int k = 0; k < e; k++){
                    if (dsu[k] == e) dsu[k] = dsu[i];
                }
            }
        }
    }
    int count[e];
    for (int i = 0; i < e; i++) count[i] = 0;
    int c = 0;
    for (int i = 0; i < e; i++) count[dsu[i]] = 1;
    for (int i = 0; i < e; i++) c+= count[i];
    if (c >= 3){
        printf("%d\n", row );
        flag += 1;
        printf("\n");
    }


}
void clear (int n, int dsu[n]){
    for (int i = 0; i < n; i++) dsu[i] = i+1;
}


int main(){
    int n,e;
    scanf("%d %d", &n, &e);

    int a[n][n];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }
    
    int u[e],v[e];
    for (int i = 0; i < e; i++){
        scanf("%d %d", &u[i], &v[i]);
    }

    int g[e][e];
    for (int i = 0; i < e; i++){
        for (int j = 0; j < e; j++) g[i][j] = 0;
    }

    new_graph(n,e,u,v,g);

    int dsu[e];

    for (int i = 0; i < e; i++){
        clear(e,dsu);
        check_connectivity(e,i,dsu,g);
    }
    if (flag == 0) printf("Not possible\n");
}
#include <stdio.h>
int max = -1000;
int min_p = 1000;

void clear (int n, int q[n], int visited[n]){
    for (int i = 0; i < n; i++){
        q[i] = 0;
        visited[i] = 0;
    }
}


void path(int n, int a[n][n], int u, int v, int visited[n], int len){
    
    if (visited[u] == 1) return;
    visited[u] = 1;

    if (a[u][v] == 1){
        len ++;
        //printf("%d\n", len);
        if (len < min_p) min_p = len;
        visited[u] = 0;
        return;
    }
    else{
        for (int j = 0; j  < n; j++){
            if (a[u][j] == 1) path(n,a,j,v,visited,len + 1);
        }
        visited[u] = 0;    
    }
    
}


void printNode (int n, int a[n][n], int visited[n], int row, int len, int q[n], int counter){
    if (len == max){
        for (int i = 0; i < counter; i++) printf("%d ", q[i]);
        printf("\n");
        q[counter - 1] = 0;
        return;
    }

    if (visited[row] == 1) return;
    visited[row] = 1;

    for (int i = 0; i < n; i++){
        if (a[row][i] == 1 && visited[i] != 1) {
            q[counter] = i;
            printNode(n,a,visited,i,len + 1, q, counter + 1);
        }
    }


}


int main(){
    int n,e;
    scanf("%d %d", &n, &e);

    int a[n][n];
    int visited[n], q[n];
    for (int i = 0; i < n; i++){
        visited[i] = 0;
        q[i] = 0;
        for (int j = 0; j < n; j++){
            a[i][j] = 0;
        }
    }

    int u,v;
    for (int i = 0; i < e; i++) {
        scanf("%d %d", &u, &v);
        a[u][v] = 1;
        a[v][u] = 1;
    }

    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            if (i != j){
                min_p = 1000;
                clear (n,q,visited);
                path(n,a,i,j,visited,0);
                if (min_p > max) {
                    max = min_p;
                    //printf("%d %d %d\n", max, i,j);
                }
            }
        }
    }
    printf("%d\n", max );

    for (int i = 0; i < n; i++){
        clear(n,q,visited);
        q[0] = i;
        printNode(n,a,visited,i,0,q,1);

    }

}
#include <stdio.h>
int max = 0;

int main(){
    int n;
    scanf("%d", &n);

    int a[n][n];
    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++){
            scanf("%d", &a[i][j]);
        }
    }

    int count = 0;
    for (int k = 1; k*k < n*n; k++){
        max = 0;
        for (int i = 0; i <= n - k; i++){
            for (int j = 0; j <= n - k; j++){
                count = 0;
                for (int r = i; r < i+k; r++){
                    for (int c = j; c < j+k; c++){
                        count += a[r][c];
                    }
                } 
                if (max < count) max = count;
            }
        }
        printf("%d\n", max );   
    }        
}
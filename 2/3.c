#include <stdio.h>
#include <stdlib.h>

int check(int n, int count[n], int row){
    if (count[row] != 0) return count[row];
    else if (count[row] == 0) return 0;

}

int bipartite(int n, int a[n][n], int row, int count[n], int grp){

    if (grp == 1) grp = 2;
    else grp = 1;

    for (int i = row; i < n; i++){
        for (int j = 0; j < n; j++){
            if (a[i][j] == 1){
                if (check(n,count,j) == 0){
                    count[j] = grp;
                    a[j][i] = 0;
                    a[i][j] = 0;
                    bipartite(n,a,j,count,grp);
                }
                else if (check(n,count,j) != grp) {
                    printf("No\n");
                    exit(0);
                }
            }
        }
    }               
    return 1;
}



int main(){
    int v;
    scanf("%d", &v);
    
    int a[v][v];
    int count[v];
    for (int i = 0; i <v; i++){
        count[i] = 0;
        for (int j = 0; j < v; j++){
            scanf("%d", &a[i][j]);
        }
    }

    count[0] = 1;
    if (bipartite(v,a,0,count,1) == 1) printf("Yes\n");


}
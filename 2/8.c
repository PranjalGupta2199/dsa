#include <stdio.h>

void printNode(int n, int a[n][n], int visited[n], int q[n], int t, int row, int counter){
    if (visited[row] == 1) return;
    visited[row] = 1; 

    if (t == 0){    
        printf("%d ", q[counter - 1]);
        return;
    }

    else {  
        for (int i = 0; i < n; i++){
            if (a[row][i] == 1 && visited[i] != 1){
                q[counter] = i;
                printNode(n,a,visited,q,t-1,i,counter +1);
            }
        }
    }
    return;


}

void clear(int n, int visited[n], int q[n]){
    for (int i = 0; i < n; i++) {
        q[i] = 0;
        visited[i] = 0;
    }
}

int main(){
    int n,e;
    scanf("%d %d", &n, &e);

    int a[n][n];
    int q[n], visited[n];
    for (int i = 0; i < n; i++){
        visited[i] = 0;
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }
    int u,v;
    for (int i = 0; i < e; i++){
        scanf("%d %d", &u, &v);
        a[u][v] = 1;
        a[v][u] = 1;
    }
    int t;
    scanf("%d", &t);

    for (int i = 0; i < n; i++){
        clear(n,visited, q);
        printf("Nodes at a distance of %d from %d ", t,i);
        q[0] = i;
        printNode(n,a,visited,q,t,i,1); 
        printf("\n");
    }
}
#include <stdio.h>
#include <limits.h>
int min = INT_MAX;

void min_weight_cycle(int n, int a[n][n], int stack[n+1], int index, int counter, int sum){
	for (int i = 0; i < counter; i++){
		if (stack[i] == index && counter >= 3) {
			sum += a[stack[counter-1]][index];
			if ( sum < min) {
				min = sum;
				for (int j = 0; j < counter; j++) printf("%d ", stack[j]);
				printf("\n");
			}
			stack[counter-1] = 0;
			return;
		}
	}
	if (counter != 1) stack[counter] = index;
	for (int j = 0; j < n; j++){
		if (a[index][j] != 0 && j != stack[counter-1]){
			min_weight_cycle(n,a,stack,j,counter+1,sum + a[index][j]);
		}
	}
	stack[counter-1] = 0;

}



void clear (int n, int visited[n], int stack[n+1]) {
	for (int i = 0; i < n; i++) {
		stack[i] = -1;
		visited[i] = 0;
	}
	stack[n] = -1;
}

int main(){
	int n,e;
	int w;

	scanf("%d %d", &n, &e);

	int a[n][n],visited[n],stack[n+1];
	for (int i = 0; i < n; i++){
		for (int j = 0;j < n; j++) a[i][j] = 0;
	}

	int u,v;
	for (int i = 0; i < e; i++){
		scanf("%d %d %d", &u, &v, &w);
		a[u][v] = w;
	}

	for (int i = 0; i < n; i++){	
		clear(n,visited,stack);
		stack[0] = i;
		min_weight_cycle(n,a,stack,i,1,0);
	}
	printf("%d\n",min );
}
#include <stdio.h>
int min = 0;

void path(int n, int a[n][n], int u, int v, int visited[n], int p_length){
    if (visited[u] == 1) return;
    visited[u] = 1;

    if (a[u][v] == 1){
        if (p_length + 1 < min) min = p_length + 1;
        return;
    }
    else{
        for (int i = 0; i < n; i++){
            if (a[u][i] == 1 && visited[i] != 1){
                path(n,a,i,v,visited,p_length + 1);
            }
        }
    }
    
    return;
}

void clear (int n, int visited[n]){
    for (int i = 0; i < n; i++) visited[i] = 0;
}

int main(){
    int n;
    scanf("%d", &n);
    
    int u,v;
    int a[n][n];

    for (int i = 0; i < n; i++){
        for (int j = 0; j < n; j++) a[i][j] = 0;
    }

    for (int i =0; i < n - 1; i++){
        scanf("%d %d", &u, &v);
        a[u][v] = 1;
        a[v][u] = 1;
    }

    int m;
    scanf("%d", &m);
    int l[m];
    for (int i = 0; i < m; i++) scanf("%d", &l[i]);

    int visited[n];
    for (int i = 0; i < n; i++) visited[i] = 0;

    int min_p = n;
    int index = 0;
    int p_length = 0;

    for (int i = 0; i < m; i++){
        min = n;
        clear(n,visited);
        path(n,a,0,l[i],visited,0);
        if (min < min_p) {
            min_p = min;
            index = i;
        }
    }
    printf("%d\n", l[index]);
}
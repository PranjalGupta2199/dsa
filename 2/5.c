#include <stdio.h>
#include <math.h>

void check_connectivity(int n, int a[n][n], int dsu[n], int node){
	int count[n];
	for (int i = 0; i < n; i++){
		dsu[i] = i+1;
		count[i] = 0;
	} 
	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			if (i != node && j != node && a[i][j] == 1) {
				int e = dsu[j];

				for (int k = 0; k < n; k++){
					if (dsu[k] == e && k != node){
						dsu[k] = dsu[i];
					}
				}
			}
		}
	}
	
	int c = 0;
	for (int i = 0; i < n; i++)count[dsu[i]] = 1;

	for (int i = 0; i < n; i++)c += count[i];

	if (c >= 3) {
		printf("\n%d\n", node);
		for (int i = 0; i < n; i++) printf("%d ", dsu[i]);
	}

}

int main(){	
	int n, d;
	scanf("%d %d", &n, &d);

	int v[n], dsu[n];
	int a[n][n];

	for (int i = 0; i < n; i++){
		v[i] = 0;
		dsu[i] = i + 1;
		for (int j = 0; j < n; j++) a[i][j] = 0;
	}

	for (int i = 0; i < n; i++){
		scanf("%d", &v[i]);
	}

	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++){
			if (fabs(v[i] - v[j]) >= d){
				a[i][j] = 1;
				a[j][i] = 1;
			}
		}
	}

	for (int i = 0; i < n; i++){
		for (int j = 0; j < n; j++)printf("%d ",a[i][j] );
		printf("\n");
	}

	for (int i = 0; i < n; i++) check_connectivity(n,a,dsu,i);
	
}
